##################################################################################
# VARIABLES
##################################################################################

variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "private_key_path" {}


##################################################################################
# PROVIDERS
##################################################################################

provider "aws" {	 
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "ap-south-1"
}

##################################################################################
# RESOURCES
##################################################################################

resource "aws_key_pair" "learner" {
	key_name = "learner"
	public_key = "${file("learner.pub")}"
}

resource "aws_security_group" "http" {
	name = "http"
#	vpc_id = "${aws.vpc.main.id}"
	
ingress {
	from_port = "80"
	to_port = "80"
	protocol = "tcp"
	cidr_blocks = ["0.0.0.0/0"]
}
ingress {
	from_port = "443"
	to_port = "443"
	protocol = "tcp"
	cidr_blocks = ["0.0.0.0/0"]
}
ingress {
        from_port = "22"
        to_port = "22"
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
}
egress {
	from_port = "0"
	to_port = "0"
	protocol = "-1"
	cidr_blocks = ["0.0.0.0/0"]
	
}

}

resource "aws_security_group" "tomcat" {
        name = "Tomcat"

ingress {
        from_port = "8080"
        to_port = "8080"
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
}
ingress {
        from_port = "22"
        to_port = "22"
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
}
egress {
        from_port = "0"
        to_port = "0"
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]

}

}


resource "aws_instance" "tomcat" {
  ami           = "ami-531a4c3c"
  instance_type = "t2.micro"
  key_name        = "learner"
  security_groups = ["Tomcat"]

  connection {
    user        = "ec2-user"
    private_key = "${file(var.private_key_path)}"
  }
 
  provisioner "remote-exec" {
    inline = [
      "sudo yum install tomcat7 tomcat7-webapps -y",
      "sudo service tomcat7 start",
      "cd /usr/share/tomcat7/webapps; sudo -u tomcat wget https://s3.amazonaws.com/infra-assessment/companyNews.war"
    ]
  }
  timeouts {
	create = "10m"
}
}

resource "aws_instance" "nginx" {
  ami           = "ami-531a4c3c"
  instance_type = "t2.micro"
  key_name        = "learner"
  security_groups = ["http"]

  connection {
    user        = "ec2-user"
    private_key = "${file(var.private_key_path)}"
  }
 
  provisioner "file" {
   source = "nginx.conf"
   destination = "/tmp/nginx.conf"

  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum install nginx -y",
      "echo ${aws_instance.tomcat.private_ip}",
      "sed -i -e 's/BACKEND_PRIVATE_IP/${aws_instance.tomcat.private_ip}/g' /tmp/nginx.conf",
      "yes|sudo cp /tmp/nginx.conf /etc/nginx/nginx.conf",
      "sudo mkdir -p /etc/pki/nginx/private",
      "sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/pki/nginx/private/server.key -out /etc/pki/nginx/server.crt  -subj '/C=IN/ST=Karnataka/L=Bangalore/O=Cloudworks/OU=IT/CN=Infra/emailAddress=test@test.com'",
      "sudo openssl dhparam -out /etc/pki/nginx/dhparams.pem 2048",
      "sudo service nginx start",
      "cd /usr/share/nginx/html; sudo wget https://s3.amazonaws.com/infra-assessment/static.zip; sudo unzip static.zip ; sudo mv static/* . ; sudo rm -rf static*"
    ]
  }
  timeouts {
	create = "10m"
 }
}
##################################################################################
# OUTPUT
##################################################################################

output "aws_instance_public_dns" {
    value = "${aws_instance.nginx.public_dns}"
}

